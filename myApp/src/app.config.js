export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/course/index',
    'pages/questionbank/index',
    'pages/learn/index',
    'pages/my/index',
    'pages/dev/index',
    'pages/course-details/index',
    'pages/learn-details/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#5875ff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  "tabBar": {
    "list": [
      {
        "pagePath": "pages/index/index",
        "iconPath": "assets/images/index.png",
        "selectedIconPath": "assets/images/index1.png",
        "text": "首页"
      },
      {
        "pagePath": "pages/course/index",
        "iconPath": "assets/images/course.png",
        "selectedIconPath": "assets/images/course1.png",
        "text": "课程"
      },
      {
        "pagePath": "pages/questionbank/index",
        "iconPath": "assets/images/questionbank.png",
        "selectedIconPath": "assets/images/questionbank1.png",
        "text": "题库"
      },
      {
        "pagePath": "pages/learn/index",
        "iconPath": "assets/images/learn.png",
        "selectedIconPath": "assets/images/learn1.png",
        "text": "学习"
      },
      {
        "pagePath": "pages/my/index",
        "iconPath": "assets/images/my.png",
        "selectedIconPath": "assets/images/my1.png",
        "text": "我的"
      }
    ]
  },
})
