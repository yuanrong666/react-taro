import React from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import unfold from '../../assets/images/unfold.png'

const Classify = () => {

  
    return (
      <View className='classify'>
        <View className='tab'>
          <View className='tableft'>
            <Text className='text'>按专业查找</Text>
            <Image className='icon' src={unfold}></Image>
          </View>

          <View className='tabright'>
            <View className='tabright1'>
              <Text className='text'>本科</Text>
              <Image className='icon' src={unfold}></Image>
            </View>

            <View className='tabright2'>
              <Text className='text'>公开课</Text>
              <Image className='icon' src={unfold}></Image>
            </View>

          </View>
        </View>
      </View>
    )

}

export default Classify
