import React from 'react'
import { View, Image, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'
import './index.scss'
import cover1 from '../../assets/images/cover1.png'
import cover2 from '../../assets/images/cover2.png'
import cover3 from '../../assets/images/cover3.png'
import cover4 from '../../assets/images/cover4.png'

const CourseList = () => {
  const list = [
    {
      id: 1,
      title: '思想道德修养与法律基础',
      class: '特色班',
      degree: '本科',
      coupons: '满500减50',
      price: '168',
      sale: '268',
      icon: cover1
    },
    {
      id: 2,
      title: '思想道德修养与法律基础',
      class: '特色班',
      degree: '本科',
      coupons: '满500减50',
      price: '168',
      sale: '268',
      icon: cover2
    },
    {
      id: 3,
      title: '思想道德修养与法律基础',
      class: '特色班',
      degree: '本科',
      coupons: '满500减50',
      price: '168',
      sale: '268',
      icon: cover3
    },
    {
      id: 4,
      title: '思想道德修养与法律基础',
      class: '特色班',
      degree: '本科',
      coupons: '满500减50',
      price: '168',
      sale: '268',
      icon: cover4
    },
    {
      id: 5,
      title: '思想道德修养与法律基础',
      class: '特色班',
      degree: '本科',
      coupons: '满500减50',
      price: '168',
      sale: '268',
      icon: cover4
    },
    {
      id: 6,
      title: '思想道德修养与法律基础',
      class: '特色班',
      degree: '本科',
      coupons: '满500减50',
      price: '168',
      sale: '268',
      icon: cover4
    }
  ]

  const goCourseDetail = (id) => {
    Taro.navigateTo({
      url: `/pages/course-details/index?id=${id}`
    })
  }

  return (
    <View className='box'>
        {list.map(item => (
          <View className='list' key={item.id} onClick={() => { goCourseDetail(item.id) }}>
            <Image className='icon' src={item.icon}></Image>
            
            <View className='right'>
              <Text className='title'>{item.title}</Text>

              <View className='degree'>
                <Text className='degree-left'>{item.class}</Text>|
                <Text className='degree-right'>{item.degree}</Text>
              </View>

              <Text className='coupons'>{item.coupons}</Text>

              <View className='price'>
                <Text className='num'>￥{item.price}</Text>
                <Text className='sale'>{item.sale}人已购买</Text>
              </View>

            </View>

          </View>
        ))}
    </View>
  )
}

export default CourseList