import React from 'react'
import './index.scss'
import { View, Image, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'

const MyOrder = () => {
  const list = [
    {
      id: 1,
      title: '订单',
      num: '6'
    },
    {
      id: 2,
      title: '优惠券',
      num: '4'
    },
    {
      id: 3,
      title: '学习卡',
      num: '3'
    },
    {
      id: 4,
      title: '开课卡',
      num: '9'
    }
  ]

  const goDetailList = (id) => {
    Taro.navigateTo({
      url: `/pages/course-details/index?id=${id}`
    })
  }

  return (
    <View className='box'>
      <View className='myorder'>
        {list.map(item => (
          <View className='list' key={item.id} onClick={() => { goDetailList(item.id) }}>
            <View className='num'>{item.num}</View>
            <View className='title'>{item.title}</View>
          </View>
        ))}
      </View>
    </View>
  )
}

export default MyOrder