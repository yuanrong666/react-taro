import React from 'react'
import './index.scss'
import { View, Image, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'
import orange from '../../assets/images/orange.png'
import blue from '../../assets/images/blue.png'
import goicon from '../../assets/images/goicon.png'

const NewCourseList = () => {
  const list = [
    {
      id: 1,
      title: '工程造价电路工程技术',
      icon: orange,
      time: '2019-07-15 09:00'
    },
    {
      id: 2,
      title: '工程造价电路工程技术',
      icon: blue,
      time: '2019-07-15 09:00'
    }
  ]

  const goNewCourseList = (id) => {
    Taro.navigateTo({
      url: `/pages/course-details/index?id=${id}`
    })
  }

  const goCourse = () => {
    Taro.navigateTo({
      url: '/pages/course/index'
    })
  }

  return (
    
    <View className='listbox'>
      <View className='toptitle'>
        <Text className='left'>最新课程</Text>
        <View className='right' onClick={() => { goCourse() }}>
          <Text className='text'>更多课程</Text>
          <Image className='goicon' src={goicon}></Image>
        </View>
      </View>
      <View className='newcourselist'>
        {list.map(item => (
          <View className='list' key={item.id} onClick={() => { goNewCourseList(item.id) }}>
            <Image className='icon' src={item.icon}></Image>
            <View className='title'>
              {item.title}
              <View className='user'>
                
                <Image className='iconuser'></Image>
                <Text className='name'>张三</Text>

                <Text className='time'>{item.time}</Text>
              </View>
            </View>
          </View>
        ))}
      </View>
    </View>
  )
}

export default NewCourseList