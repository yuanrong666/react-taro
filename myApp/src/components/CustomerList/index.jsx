import React from 'react'
import './index.scss'
import { View, Image, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'
import customer from '../../assets/images/customer.png'
import feedback from '../../assets/images/feedback.png'
import set from '../../assets/images/set.png'
import back from '../../assets/images/back.png'

const CustomerList = () => {
  const list = [
    {
      id: 1,
      title: '联系客服',
      icon: customer
    },
    {
      id: 2,
      title: '我的反馈',
      icon: feedback
    },
    {
      id: 3,
      title: '设置',
      icon: set
    }
  ]

  const goCustomerList = (id) => {
    Taro.navigateTo({
      url: `/pages/course-details/index?id=${id}`
    })
  }

  return (
    <View className='box'>
      <View className='customerlist'>
        {list.map(item => (
          <View className='list' key={item.id} onClick={() => { goCustomerList(item.id) }}>
            <View className='left'>
              <Image className='icon' src={item.icon}></Image>
              <Text className='title'>{item.title}</Text>
            </View>
            <Image className='iconright' src={back}></Image>
          </View>
        ))}
      </View>
    </View>
  )
}

export default CustomerList