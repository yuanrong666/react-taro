import React from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import archives from '../../assets/images/archives.png'

const Archives = () => {

    return (
      <View className='box'>
        <View className='arc'>
          <Text className='text'>我的档案</Text>
          <Text className='time'>存档于2019年5月28日</Text>
        </View>
      </View>
    )

}

export default Archives
