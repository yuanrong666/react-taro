import React from 'react'
import { View, Text, Image, Video, } from '@tarojs/components'
import './index.scss'
import video from '../../assets/images/video.png'
const VideoPlugins = () => {

    return (
      <View className='videobox'>
        <Video className='video' poster={video} src='' ></Video>
      </View>
    )

}

export default VideoPlugins
