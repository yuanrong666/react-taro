import React from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

const CourseIntroduction = () => {
  const list = [
    {
      id: 1,
      title: '课程简介',
      copy: '计算机上机实操与理论知识点讲解'
    },
    {
      id: 2,
      title: '课程时长',
      copy: '60分钟'
    }
  ]
    return (
      <View className='introduction'>
        {list.map(item => (
          <View className='hezi'>
            <Text className='title'>{item.title}</Text>
            <Text className='copy'>{item.copy}</Text>
          </View>
        ))}
      </View>
    )

}

export default CourseIntroduction
