import React from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import bofang from '../../assets/images/bofang.png'
import xiazai from '../../assets/images/xiazai.png'
const DirectoryList = () => {
  const list = [
    {
      id: 1,
      title: '计算机上机实操与理论知识点讲解',
      progress: '60'
    },
    {
      id: 2,
      title: '计算机上机实操与理论知识点讲解',
      progress: '70'
    },
    {
      id: 3,
      title: '计算机上机实操与理论知识点讲解',
      progress: '80'
    },
    {
      id: 4,
      title: '计算机上机实操与理论知识点讲解',
      progress: '90'
    }
  ]

    return (
      <View className='box'>
        {list.map(item => (
          <View className='list'>
            <Image className='iconleft' src={bofang}></Image>
            <View className='zhong'>
              <Text className='title'>{item.title}</Text>
              <Text className='progress'>已学 {item.progress}%</Text>
            </View>
            <Image className='iconright' src={xiazai}></Image>
          </View>
        ))}
      </View>
    )

}

export default DirectoryList
