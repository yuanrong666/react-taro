import React from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

const UserInfo = () => {

    return (
      <View className='user'>
        <View className='box'>
          <Image className='avatar'></Image>
          <Text className='name'>包小图</Text>
          <Text className='balance'>账户余额：￥1200</Text>
        </View>
      </View>
    )

}

export default UserInfo
