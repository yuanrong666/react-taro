import React, { useEffect, useState } from 'react'
import classNames from 'classnames'
import { View, Text, Image } from '@tarojs/components'
import styles from './index.scss'
import './index.scss'

const CourseTabs = () => {
  const [currentIdx, setCurrentIdx] = useState(0)
  
  const tablist = [
    {
      id: 0,
      value: '课程详情'
    },
    {
      id: 1,
      value: '师资介绍'
    },
    {
      id: 2,
      value: '学员评价'
    }
  ]
  
  const toChangeTab = (idx) => {
    if (currentIdx == idx) return
    console.log('idx---', idx)
    setCurrentIdx(idx)
  }

    return (
      <View className='listbox'>
        <View className='list'>
          {
            tablist.map((item, index) => {
              // return <Text className={classNames(styles.text, {[styles.active_text]: currentIdx === index})} onClick={() => toChangeTab(index)} key={item.id}>{ item.value }</Text>
              return <Text 
                        className={currentIdx === index ? 'active-text' : 'text'}
                        onClick={() => toChangeTab(index)} 
                        key={item.id}
                      >{ item.value }
                      </Text>
            })
          }
        </View>
      </View>
    )

}

export default CourseTabs
