import React from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import computer from '../../assets/images/computer.png'
import icon1 from '../../assets/images/icon1.png'
import icon2 from '../../assets/images/icon2.png'
import icon3 from '../../assets/images/icon3.png'

const CourseInformation = () => {

    return (
      <View className='box'>
        <Image className='img' src={computer}></Image>

        <View className='information'>
          <Text className='title'>计算机上机实操与理论知识点讲解</Text>
          <View className='list'>
            <View className='specialized'>
              <Image className='icon1' src={icon1}></Image>
              <Text className='text1'>整专业</Text>
            </View>

            <View className='cart'>
              <Image className='icon2' src={icon2}></Image>
              <Text className='text2'>235人购买</Text>
            </View>

            <View className='like'>
              <Image className='icon3' src={icon3}></Image>
              <Text className='text3'>438人喜欢</Text>
            </View>
          </View>
        </View>

        <View className='price'>
          <Text className='num'>￥168.00</Text>
          <Text className='minus'>满500减50</Text>
        </View>

      </View>
    )

}

export default CourseInformation
