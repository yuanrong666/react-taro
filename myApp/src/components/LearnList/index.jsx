import React from 'react'
import { View, Image, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'
import './index.scss'
import video1 from '../../assets/images/video1.png'
import cover2 from '../../assets/images/cover2.png'
import cover3 from '../../assets/images/cover3.png'
import cover4 from '../../assets/images/cover4.png'
import enter from '../../assets/images/enter.png'

const LearnList = () => {
  const list = [
    {
      id: 1,
      title: '思想道德修养与法律基础',
      time: '60',
      uptime: '8',
      icon: video1
    },
    {
      id: 2,
      title: '思想道德修养与法律基础',
      time: '60',
      uptime: '8',
      icon: cover2
    },
    {
      id: 3,
      title: '思想道德修养与法律基础',
      time: '60',
      uptime: '8',
      icon: video1
    },
    {
      id: 4,
      title: '思想道德修养与法律基础',
      time: '60',
      uptime: '8',
      icon: cover3
    },
    {
      id: 5,
      title: '思想道德修养与法律基础',
      time: '60',
      uptime: '8',
      icon: video1
    },
    {
      id: 6,
      title: '思想道德修养与法律基础',
      time: '60',
      uptime: '8',
      icon: cover4
    }
  ]

  const goCourseDetail = (id) => {
    Taro.navigateTo({
      url: `/pages/learn-details/index?id=${id}`
    })
  }

  return (
    <View className='box'>
        {list.map(item => (
          <View className='list' key={item.id} onClick={() => { goCourseDetail(item.id) }}>
            <Image className='icon' src={item.icon}></Image>
            
            <View className='right'>
              <Text className='title'>{item.title}</Text>

              <View className='time'>
                <Text className='timetext'>学习时间 {item.time}分钟</Text>
              </View>

              <View className='time'>
                <Text className='timetext'>已开始 {item.uptime}分钟</Text>
              </View>

              <Image className='img' src={enter}></Image>
            </View>

          </View>
        ))}
    </View>
  )
}

export default LearnList