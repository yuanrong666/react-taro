import React from 'react'
import './index.scss'
import { View, Image, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'
import recommend from '../../assets/images/recommend.png'
import subscribe from '../../assets/images/subscribe.png'
import collection from '../../assets/images/collection.png'
import download from '../../assets/images/download.png'

const Testimonials = () => {
  const list = [
    {
      id: 1,
      title: '名师推荐',
      icon: recommend
    },
    {
      id: 2,
      title: '订阅专栏',
      icon: subscribe
    },
    {
      id: 3,
      title: '我的收藏',
      icon: collection
    },
    {
      id: 4,
      title: '下载管理',
      icon: download
    }
  ]

  const goDetailList = (id) => {
    Taro.navigateTo({
      url: `/pages/order/order-list/index?id=${id}`
    })
  }

  return (
    <View className='box'>
      <View className='testimonials'>
        {list.map(item => (
          <View className='list' key={item.id} onClick={() => { goDetailList(item.id) }}>
            <Image className='icon' src={item.icon}></Image>
            <View className='title'>{item.title}</View>
          </View>
        ))}
      </View>
    </View>
  )
}

export default Testimonials