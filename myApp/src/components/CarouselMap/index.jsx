import React, { useState, useEffect } from 'react'
import { View, Text, Image, Swiper, SwiperItem } from '@tarojs/components'
import './index.scss'
import ad from '../../assets/images/ad.png'
import ad2 from '../../assets/images/ad2.png'

const CarouselMap = () => {



  useEffect(() => {
    setTimeout(() => {

    }, 300)
  }, [])

    return (
      <View className='carouselmap'>
        <View className='imgbox'>
          <Swiper className=''
          indicatorColor=''
          indicatorActiveColor=''
          circular
          indicatorDots
          autoplay
          >
            <SwiperItem>
              <Image className='img' src={ad}></Image>
            </SwiperItem>
            <SwiperItem>
              <Image className='img' src={ad2}></Image>
            </SwiperItem>
          </Swiper>
        </View>
      </View>
    )

}

export default CarouselMap
