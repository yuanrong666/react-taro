import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import CourseInformation from '../../components/CourseInformation'
import CourseIntroduction from '../../components/CourseIntroduction'
import CourseTabs from '../../components/CourseTabs'

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='course'>
        <CourseInformation />
        <CourseTabs />
        <CourseIntroduction />

      </View>
    )
  }
}
