import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import VideoPlugins from '../../components/VideoPlugins'
import DirectoryList from '../../components/DirectoryList'
import LearnTabs from '../../components/LearnTabs'
export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='detailsbox'>
        <VideoPlugins />
        <LearnTabs />
        <DirectoryList />

      </View>
    )
  }
}
