import { Component } from 'react'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import Testimonials from '../../components/testimonials'
import NewCourseList from '../../components/NewCourseList'
import RecommendCourseList from '../../components/RecommendCourseList'
import CarouselMap from '../../components/CarouselMap'

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        {/**顶部广告 */}
        <CarouselMap />
        {/* <View className='ad'></View>   */}

        {/**推荐 */}
        <Testimonials />

        {/**最新课程列表 */}
        <NewCourseList />

        {/**推荐课程列表 */}
        <RecommendCourseList />
      </View>
    )
  }
}
