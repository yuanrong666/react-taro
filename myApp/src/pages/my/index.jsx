import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import UserInfo from '../../components/UserInfo'
import MyOrder from '../../components/MyOrder'
import Archives from '../../components/Archives'
import CustomerList from '../../components/CustomerList'
export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <View className='mybox'>
          <UserInfo />
          <MyOrder />
          <Archives />
          <CustomerList />

        </View>
      </View>
    )
  }
}
