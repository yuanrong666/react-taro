import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import CourseList from '../../components/CourseList'
import Classify from '../../components/Classify'

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <Classify />
        <CourseList />
      </View>
    )
  }
}
